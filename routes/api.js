const express = require('express');
const jsonwebtoken = require('jsonwebtoken');
const router = express.Router();
const User = require('../models/user');
const mongoose = require('mongoose');

const dbUrl = 'mongodb+srv://mean0user:mean0pass@mean0-p6cfp.mongodb.net/eventsdata?retryWrites=true&w=majority';

mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
  if (err) {
    console.log('Error: ', err);
  } else {
    console.log('Connected to db');
  }
});

function verifyTJwt(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).send('This request requires authorization');
  }
  
  const jwt = req.headers.authorization.split(' ')[1];
  if ('null' === jwt) {
    return res.status(401).send('Unauthorized request');
  }
  
  const jwtPayload = jsonwebtoken.verify(jwt, 'secretKey');
  if (!jwtPayload) {
    return res.status(401).send('Unauthorized request');
  }
  
  req.userId = jwtPayload.subject;
  next();
}

router.get('/', (req, res) => {
  res.send('Hello, from API router')
});

router.post('/register', (req, res) => {
  let userData = req.body;
  
  if (userData.password === userData.passwordConfirmation) {
    let user = new User(userData);
  
    user.save((err, savedUser) => {
      if (err) {
        console.log('Error: ', err);
      } else {
        const jwtPayload = {
          subject: savedUser._id
        };
        const jwt = jsonwebtoken.sign(jwtPayload, 'secretKey');
        res.status(201).send({jwt});
      }
    });
  } else {
    res.status(400).send({err: 'Provided passwords doesn\'t match'});
  }
});

router.post('/login', (req, res) => {
  let userData = req.body;
  User.findOne({email: userData.email}, (err, user) => {
    console.log('findOne: ', err, user);
    if (err) {
      console.log('Error while searching user:', err);
    }
    else if (!user || user.password !== userData.password) {
      res.status(401).send({err: 'Invalid username or password'});
    }
    else {
      const jwtPayload = {
        subject: user._id
      };
      const jwt = jsonwebtoken.sign(jwtPayload, 'secretKey');
      res.status(200).send({jwt});
    }
  });
});

router.get('/events', (req, res) => {
  let events = [
    {"_id": "1", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"},
    {"_id": "2", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"},
    {"_id": "3", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"},
    {"_id": "4", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"},
    {"_id": "5", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"}
  ];
  
  res.json(events);
});

router.get('/special', verifyTJwt, (req, res) => {
  let events = [
    {"_id": "1", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"},
    {"_id": "2", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"},
    {"_id": "3", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"},
    {"_id": "4", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"},
    {"_id": "5", "name": "Auto Expo", "description": "lorem ipsum", "date": "2012-04-23T18:25:43"}
  ];
  
  res.json(events);
});

module.exports = router;
